#ifndef ADD_ENTRY_HPP
#define ADD_ENTRY_HPP

#include <opencv2/opencv.hpp>
#include <string>
#include "frame_metainf.hpp"
#include "file_config.hpp"

//struct frame_metainf: represents information about frame to add to the ressources file
/*
***** this function acces ressource file to add a new entry, otherwise if the file is not created, initiate it and add the entry,
uses file_config, and the config_milestone.
*/

void add_entry(cv::Mat frame, frame_metainf metainf, config_milestone *config_, const char * ressource_file);

#endif