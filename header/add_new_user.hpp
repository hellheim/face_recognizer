#ifndef ADD_NEW_USER_HPP
#define ADD_NEW_USER_HPP

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config_struct.hpp"
#include "config_socket.hpp"
#include "error_.hpp"
#include "protocol.hpp"

bool add_new_user(int fd);

#endif