#ifndef FRAME_SIZE_HPP
#define FRAME_SIZE_HPP
#ifndef WIDTH
#define WIDTH 640
#endif
#ifndef HEIGHT
#define HEIGHT 480
#endif

int frame_size(int type);

#endif