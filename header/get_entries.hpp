#ifndef GET_ENTRIES_HPP
#define GET_ENTRIES_HPP

#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include "read_entry.hpp"
#include "error_.hpp"

bool get_entries(const char * ressource_file, std::vector<cv::Mat> *frames, std::vector<int> *id);

#endif