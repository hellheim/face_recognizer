#ifndef READ_ENTRY_HPP
#define READ_ENTRY_HPP

#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include "frame_metainf.hpp"

#define SEPARATOR ","

frame_metainf read_entry(std::string ressource_file_line);

#endif