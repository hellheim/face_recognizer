#ifndef SEND_ID_HPP
#define SEND_ID_HPP

#include "config_struct.hpp"
#include "config_socket.hpp"
#include "error_.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void send_id(config_struct_local config_, int user_id);

#endif