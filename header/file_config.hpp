#ifndef FILE_CONFIG_HPP
#define FILE_CONFIG_HPP

#include <iostream>
#include <fstream>
#include "error_.hpp"
#include "config_milestone.hpp"

#define F_UPDATE true

//struct config_milestone: struct contains last frame number stored and last id stored
/*
***** this functions retrieves last frame number and last id stored if config_file exists
***** else, it creates the file with 0 values
***** in case the flag is set to update, it updates the config_file according to the struct
*/
void file_config(const char * config_file, config_milestone *config_, bool flag);

#endif