#ifndef CONFIG_SOCKET_SEND_HPP
#define CONFIG_SOCKET_SEND_HPP

#include "config_struct.hpp"
#include "error_.hpp"

//configure socket for remote communication
void config_socket_send(config_struct_local *config_, const char* socket_name);

#endif