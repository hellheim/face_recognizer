#ifndef RETRIEVE_FRAMES_HPP
#define RETRIEVE_FRAMES_HPP
#include <opencv2/opencv.hpp>
#include "config_struct.hpp"
#include "config_socket.hpp"
#include <stdio.h>

#define SEND_STREAM "send_stream"
#define ACCEPT_STREAM "streaming_"
#define STOP_STREAM "stop_stream"

void retrieve_frames(std::vector<cv::Mat> *frames, int client_sockfd, bool *siglistner);

#endif