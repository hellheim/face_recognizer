#ifndef CONFIG_STRUCT_HPP
#define CONFIG_STRUCT_HPP

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>


typedef struct {
    struct sockaddr_un serv_addr;
    struct sockaddr_un client_addr;
    char socket_name[20];
    int sockfd;
}config_struct;

typedef struct {
    struct sockaddr_un serv_addr;
    struct sockaddr_un client_addr;
    char socket_name[20];
    int sockfd;
}config_struct_local;

#endif