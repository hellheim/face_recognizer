#include "header/add_new_user.hpp"

bool add_new_user(int fd)
{
    char buffer[12];
    int n;
    bzero(buffer,sizeof(buffer));
    strcpy(buffer,ASK);
    n = write(fd,buffer,sizeof(buffer));
    if(n < 0)
        error_("Can't write asking request to socket..");
    bzero(buffer, sizeof(buffer));
    n = read(fd,buffer,sizeof(buffer));
    if(n < 0)
        error_("Can't read from socket..");
    if(strcmp(buffer,ADD) == 0)
        return true;
    else
        return false;
}