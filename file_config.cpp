#include "header/file_config.hpp"

void file_config(const char * config_file, config_milestone *config_, bool flag = false)
{
    std::fstream file;
    std::string buffer;
    int nl=0;
    if(!flag)
    {
        file.open(config_file,std::ios::in);
        if(file.is_open())
        {
            for(nl = 0; std::getline(file,buffer);++nl);
            if(nl != 2)
            {
                file.close();
                error_((std::string)"Error in config_file.");
            }
            else
            {
                file.close();
                file.open(config_file,std::ios::in);
                nl=0;
                while(!file.eof())
                {
                    getline(file,buffer);
                    switch(nl)
                    {
                        case 0:
                            config_->frame = std::stoi(buffer,nullptr,10);
                            break;
                        case 1:
                            config_->id = std::stoi(buffer,nullptr,10);
                            break;
                    }
                    nl++;
                }
            }
        }
        else
        {
            file.open(config_file,std::ios::out);
            if(file.is_open())
            {
                file << 0 << std::endl << 0;
                config_->id = 0;
                config_->frame = 0;
            }
        }
        file.close();
    }
    else
    {
        file.open(config_file,std::ofstream::out | std::ofstream::trunc);
        if(file.is_open())
        {
            file << config_->frame << std::endl << config_->id;
        }
        file.close();
    }
}