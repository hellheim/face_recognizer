#include "header/read_entry.hpp"

frame_metainf read_entry(std::string ressource_file_line)
{
    frame_metainf metainf;
    int id, frame_label;
    std::string tmp1, tmp2, tmp3;
    size_t pos = ressource_file_line.find(SEPARATOR);
    tmp1 = ressource_file_line.substr(0,pos);
    tmp2 = ressource_file_line.substr(pos+strlen(SEPARATOR),strlen(ressource_file_line.c_str()));
    tmp3 = tmp1.substr(tmp1.find("img/img")+strlen("img/img"),tmp1.find(".jpeg"));
    id = std::stoi(tmp2);
    frame_label = std::stoi(tmp3);
    metainf.frame_label = frame_label;
    metainf.id = id;
    return metainf;
}