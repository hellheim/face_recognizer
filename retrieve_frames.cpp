#include "header/retrieve_frames.hpp"
#include "header/frame_size.hpp"
#include "header/error_.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>

bool is_numerical(char *string)
{
    if(string[0] >= '0' && string[0] <= '9')
        return true;
    else
        return false;
}


void retrieve_frames(std::vector<cv::Mat> *frames, int client_sockfd, bool *siglistner)
{
    char buffer[12];
    int n, nframes;
    int imgSize, width_, height_;
    uchar *sockData;
    do
    {
        bzero(buffer,sizeof(buffer));
        n = read(client_sockfd,buffer,sizeof(buffer));
        if(n <0)
            error_("Unable to read from socket.." );
        if(strcmp(buffer,"exit") == 0)
        {
            *siglistner = true;
        }
    }while(!is_numerical(buffer) && *siglistner != true);
    if(*siglistner != true)
    {
        nframes = std::stoi(std::string(buffer),nullptr,10);
        std::cout << "Recieving "<< nframes <<" frames.." << std::endl;
        bzero(buffer,sizeof(buffer));
        strcpy(buffer,SEND_STREAM);
        n = write(client_sockfd,buffer,strlen(buffer));
        n = 0;
        for(int i = 0; i < nframes; i++)
        {
            std::cout << "frame n°" << i << std::endl;
            bzero(buffer,sizeof(buffer));
            //recieving width
            n = read(client_sockfd,buffer, sizeof(buffer));
            if(n < 0)
            error_("ERROR recieving width with target..");
            width_ = std::atoi(buffer);
            /******************************/
            //continuing
            std::cout << "recieved width.." << std::endl;
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,SEND_STREAM);
            n = write(client_sockfd,buffer,strlen(buffer));
            /******************************/
            //recieving height
            bzero(buffer,sizeof(buffer));            
            n = read(client_sockfd,buffer, sizeof(buffer));
            if(n < 0)
                error_("ERROR recieving width with target..");
            height_ = std::atoi(buffer);
            /******************************/
            //continuing
            std::cout << "recieved height.." << std::endl;
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,SEND_STREAM);
            n = write(client_sockfd,buffer,strlen(buffer));
            /******************************/
            //recieving imgSize
            bzero(buffer,sizeof(buffer));            
            n = read(client_sockfd,buffer, sizeof(buffer));
            if(n < 0)
                error_("ERROR recieving width with target..");
            imgSize = std::atoi(buffer);
            /******************************/
            std::cout << "recieved imgSize.." << std::endl;
            sockData = (uchar *)malloc(sizeof(uchar)*imgSize);
            bzero(sockData,sizeof(sockData));
            //continuing
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,SEND_STREAM);
            n = write(client_sockfd,buffer,strlen(buffer));
            /******************************/
            n = 0;
            std::cout << imgSize << std::endl;
            for(int j = 0; j < imgSize; j += n)
            {
                n = recv(client_sockfd,sockData + j, imgSize - j,0);
                if(n < 0)
                {
                    error_("Unable to read from socket..");
                }
            }
            cv::Mat img = cv::Mat(cv::Size(height_ , width_), CV_8UC1, sockData);
            cv::Mat tmp = img.clone();
            frames->push_back(tmp);
            free(sockData);
            bzero(buffer,sizeof(buffer));
            if(i < nframes-1)
            {
                strcpy(buffer,SEND_STREAM);
                write(client_sockfd,buffer,strlen(buffer));
            }                  
        }
        bzero(buffer,sizeof(buffer));
        strcpy(buffer,STOP_STREAM);
        write(client_sockfd,buffer,strlen(buffer));
        std::cout << "End recieving.." << std::endl;
    }
    else
    {
        exit(0);
    }
}
