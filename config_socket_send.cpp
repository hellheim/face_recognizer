#include "header/config_socket_send.hpp"

void config_socket_send(config_struct_local *config_, const char* socket_name)
{
    int n;
    strcpy(config_->socket_name ,socket_name);
    config_->sockfd = socket(PF_LOCAL,SOCK_STREAM,0);
    config_->serv_addr.sun_family = AF_LOCAL;
    strcpy(config_->socket_name,socket_name);
    strcpy(config_->serv_addr.sun_path,config_->socket_name);
    socklen_t len = sizeof(config_->serv_addr);
    n = connect(config_->sockfd ,(struct sockaddr * )&(config_->serv_addr),len);
    if(n < 0)
    {
        error_("Unable to connecti to socket");
    }
}