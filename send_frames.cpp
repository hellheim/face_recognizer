#include "header/send_frames.hpp"
#include "header/frame_size.hpp"
#include "header/error_.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>


void send_frames(std::vector<cv::Mat> *frames, int serv_sockfd, const char *nof)
{
    char buffer[12];
    int client_sockfd, n, nframes = std::stoi(nof);
    cv::Mat img;
    int imgSize = frame_size(CV_8UC3);
    uchar sockData[imgSize];
    bzero(sockData,sizeof(sockData));
    bzero(buffer,sizeof(buffer));
    if(n < 0)
    {
        error_("Unable to connecti to socket");
    }
    bzero(buffer,sizeof(buffer));
    strcpy(buffer,nof);
    n = write(serv_sockfd,buffer,strlen(buffer));
    if(n < 0)
    {
        error_("Unable to recieve");
    }
    bzero(buffer,sizeof(buffer));
    n = read(serv_sockfd,buffer,sizeof(buffer));
    if(n < 0)
    {
        error_( "Unable to read from socket");
    }
    if(strcmp(buffer,SEND_STREAM)==0)
    {
        for(int i = 0; i < nframes; i++)
        {
            bzero(buffer,sizeof(buffer));
            img = cv::Mat(frames->at(i));
            img = img.reshape(0,1);
            imgSize = img.total()*img.elemSize();
            n = send(serv_sockfd,img.data,imgSize,0);
            if(n < 0)
                error_("ERROR sending image to target..");
            bzero(buffer,sizeof(buffer));
            n = read(serv_sockfd,buffer,sizeof(buffer));
            if(strcmp(buffer,SEND_STREAM) == 0)
            {
                std::cout << ">";
            }
            else
            {
                error_("stop sending..");
            }
        }
    }
}
