#include "header/sigint_listner.hpp"

void sigint_listner(int fd, bool *siglist)
{
    char buffer[12];
    int n;
    do
    {
        bzero(buffer, sizeof(buffer));
        n = read(fd, buffer, sizeof(buffer));
        if(n < 0)
            error_("SIGINT_LISTNER: Can't read from socket");
    }while((strcmp(buffer,EXIT) != 0) && (*siglist != true) );
    *siglist = true;
    std::cout << "Closing program.." << std::endl;
    exit(0);
}