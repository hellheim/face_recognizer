/***********

this programe requires this architecture:
face_recognizer -> binary file
cnf/config_file -> config file conatining latest User id and latest frame number
cnf/ressource_file -> csv file containing the database entries with their corresponding User id

*/
#include <iostream>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>
#include <opencv2/objdetect.hpp>
#include "header/file_config.hpp"
#include "header/config_struct.hpp"
#include "header/read_entry.hpp"
#include "header/add_entry.hpp"
#include "header/retrieve_frames.hpp"
#include "header/send_id.hpp"
#include "header/get_entries.hpp"
#include "header/config_socket.hpp"
#include "header/config_socket_send.hpp"
#include "header/initi_pcontrol.hpp"
#include "header/sig_handler.hpp"
#include "header/sigint_listner.hpp"
#include "header/add_new_user.hpp"

#define CONFIG_FILE "cnf/config_file"
#define RESSOURCE_FILE "cnf/ressource_file"
#define THRESHOLD 50.0

using namespace std;
using namespace cv;
using namespace cv::face;

int fd_sig_handler = 0;

int main(int argc, char **argv)
{
    if(argc < 4)
    {
        error_("Please use like: face_recognizer <socket-name> <socket-config-user> <socket-config>");
    }
    config_milestone config_;
    config_struct_local config_local, config_control, config_control_;
    vector<cv::Mat> frames;
    vector<cv::Mat> new_frames;
    int client_sockfd, n;
    int *predictedId;
    char buffer[12];
    double *confidence, avg = 0.0;
    string control;
    Ptr<LBPHFaceRecognizer> model = LBPHFaceRecognizer::create();
    //Ptr<EigenFaceRecognizer> model = EigenFaceRecognizer::create();
    vector<int> ids;
    bool file_state, siglistner = false, add_nu = false;
    config_socket_send(&config_control_,argv[3]);
    initi_pcontrol(config_control_.sockfd);
    fd_sig_handler = config_control.sockfd;
    signal(SIGINT,sig_handler);
    //thread worker(sigint_listner,config_control.sockfd,&siglistner);
    file_config(CONFIG_FILE,&config_,false);
    config_socket(&config_local, argv[1], &client_sockfd);
    config_socket_send(&config_control,argv[2]);
    file_state = get_entries(RESSOURCE_FILE,&frames,&ids);
    if(file_state)
    {
        cout << "loaded frames from database." << endl << "Generating model.." << endl;
        model->train(frames,ids);
    }
    while(!siglistner)
    {
        avg = 0.0;
        bzero(buffer,sizeof(buffer));
        strcpy(buffer,"finished");
        n = write(config_control_.sockfd,buffer,sizeof(buffer));
        if(n <0)
            error_("Unable to read from socket.." );
        retrieve_frames(&new_frames,client_sockfd, &siglistner);
        while(!file_state)
        {
            cout << file_state << endl;
            cout << "No current entries in database: " << endl << "Adding new user to database under ID_usr" << config_.id+1 << endl;
            config_.id += 1;
            frame_metainf metainf;
            metainf.id = config_.id;
            for(size_t i = 0; i < new_frames.size(); i++)
            {
                add_entry(new_frames.at(i),metainf,&config_,RESSOURCE_FILE);
            }
            cout << "Added new entries to databse.." << endl << "Retrieving one more time.." << endl;
            file_state = get_entries(RESSOURCE_FILE,&frames,&ids);
            if(file_state)
            {
                cout << "loaded frames from database." << endl << "Generating model.." << endl;
                model->train(frames,ids);
            }
        }
        //allocating prediction and confidence array
        predictedId = (int *)malloc(new_frames.size()*sizeof(int));
        confidence = (double *)malloc(new_frames.size()*sizeof(double));
        cout << "Recognizing frames.." << endl;
        for(int i = 0; i < new_frames.size(); i++)
        {
            model->predict(new_frames.at(i),predictedId[i],confidence[i]);
            cout << "frame "<< i <<"-:Predicted Id: ID_usr"<< predictedId[i]<< " with "<<confidence[i]<<" confidence" <<endl;
            avg = avg + confidence[i];
        }
        avg = avg / new_frames.size();
        //if under threshold the software adds the new frames to database for more relience model
        if(avg <= THRESHOLD)
        {
            cout << "Adding new_frames to database with id ID_usr"<< predictedId[0] << endl;
            frame_metainf metainf_;
            send_id(config_control,predictedId[0]);//sending Id to get_config_user
            metainf_.id = predictedId[0];
            for(size_t i = 0; i < new_frames.size(); i++)
            {
                add_entry(new_frames.at(i),metainf_,&config_,RESSOURCE_FILE);
            }
            frames.clear();
            ids.clear();
            cout << "Added new entries to databse.." << endl << "Retrieving one more time.." << endl;
            file_state = get_entries(RESSOURCE_FILE,&frames,&ids);
            if(file_state)
            {
                cout << "loaded frames from database." << endl << "Generating model.." << endl;
                model->train(frames,ids);
            }
        }
        else
        {
            send_id(config_control,0);//sending Id to get_config_user
            add_nu = add_new_user(config_control_.sockfd);
            if(add_nu)
            {
                config_.id += 1;
                frame_metainf metainf;
                metainf.id = config_.id;
                for(size_t i = 0; i < new_frames.size(); i++)
                {
                    add_entry(new_frames.at(i),metainf,&config_,RESSOURCE_FILE);
                }
                cout << "Added new entries to databse.." << endl << "Retrieving one more time.." << endl;
                file_state = get_entries(RESSOURCE_FILE,&frames,&ids);
                if(file_state)
                {
                    cout << "loaded frames from database." << endl << "Generating model.." << endl;
                    model->train(frames,ids);
                }
                send_id(config_control,config_.id);//sending Id to get_config_user
            }
        }
        /******** updating config file *********/
        new_frames.clear();
        free(predictedId);
        free(confidence);
        file_config(CONFIG_FILE,&config_,true);
    }
    return 0;
}