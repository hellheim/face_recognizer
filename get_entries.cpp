#include "header/get_entries.hpp"

bool get_entries(const char * ressource_file, std::vector<cv::Mat> *frames, std::vector<int> *id)
{
    std::fstream file;
    cv::Mat img;
    std::string buffer, buffer2;
    frame_metainf metainf;
    file.open(ressource_file,std::ios::in);
    if(file.is_open())
    {
        if(!file.eof())
        {
            while(!file.eof())
            {
                std::getline(file,buffer);
                if(!buffer.empty())
                {
                    metainf = read_entry(buffer);
                    id->push_back(metainf.id);
                    buffer2 = buffer.substr(0,buffer.find(SEPARATOR));
                    img = cv::imread(buffer2, cv::IMREAD_GRAYSCALE);
                    cv::Mat tmp = img.clone();
                    frames->push_back(tmp);
                }
            }
            file.close();
            if((frames->size() > 0) && (id->size() > 0))
                return true;
            else
                return false;
        }
        else
        {
            std::cout << "ressource_file is empty." << std::endl;
            file.close();
            return false;
        }
    }
    else
    {
        std::cout << "ressource_file inexistant" << std::endl;
        file.close();
        return false;
    }
}