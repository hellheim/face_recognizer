#include "header/send_id.hpp"

void send_id(config_struct_local config_, int user_id)
{
    char buffer[12];
    int n = 0;
    sprintf(buffer,"%d",user_id);
    n = write(config_.sockfd,buffer,sizeof(buffer));
    if(n < 0)
    {
        error_("Cannot write id to socket");
    }
}