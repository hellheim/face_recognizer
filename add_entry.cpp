#include "header/add_entry.hpp"

void add_entry(cv::Mat frame, frame_metainf metainf ,config_milestone *config_, const char * ressource_file)
{
    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(0);
    std::fstream file;
    std::string buffer;
    file.open(ressource_file,std::ios::out | std::ios::app);
    if(file.is_open())
    {
        config_->frame += 1;
        file << "img/img" << config_->frame << ".png," << metainf.id << std::endl;
        file.close();
        buffer = "img/img"+std::to_string(config_->frame)+".png";
        cv::imwrite(buffer,frame,params);
    }
}
